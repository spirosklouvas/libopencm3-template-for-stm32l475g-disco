Libopencm3 template/example for the STM32L475 Discovery IOT board (B-L475E-IOT01A2).
Based on https://github.com/libopencm3/libopencm3-template.git

-----

Easy "clone and go" repository for a libopencm3 based project.

# Instructions
 1. git clone --recurse-submodules https://github.com/libopencm3/libopencm3-template.git your-project
 2. cd your-project
 3. make -C libopencm3 # (Only needed once)
 4. make -C project
 5. Remove examples if you do not need them

If you have an older git, or got ahead of yourself and skipped the ```--recurse-submodules```
you can fix things by running ```git submodule update --init``` (This is only needed once)

# Directories
* project contains your application

## Examples:
* stm32l475g-miniblink
* stm32l475g-basic-button
* stm32l475g-timerblink
* stm32l475g-usart

# As a template
You should replace this with your _own_ README if you are using this
as a template.
