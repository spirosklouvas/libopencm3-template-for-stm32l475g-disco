#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/usart.h>

#define LED_1_PORT GPIOA
#define LED_1_PIN GPIO5

#define USART_CONSOLE USART1  /* PB6/7 , af7 */

int _write(int file, char *ptr, int len);
void read_from_console(char* str, int count);

static void clock_setup(void)
{
    /* FIXME - this should eventually become a clock struct helper setup */
    rcc_osc_on(RCC_HSI16);

    flash_prefetch_enable();
    flash_set_ws(4);
    flash_dcache_enable();
    flash_icache_enable();
    /* 16MHz / 4 = > 4 * 40 = 160MHz VCO => 80MHz main pll  */
    rcc_set_main_pll(RCC_PLLCFGR_PLLSRC_HSI16, 4, 40,
            0, 0, RCC_PLLCFGR_PLLR_DIV2);
    rcc_osc_on(RCC_PLL);
    /* either rcc_wait_for_osc_ready() or do other things */

    /* Enable clocks for the ports we need */
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);

    /* Enable clocks for peripherals we need */
    rcc_periph_clock_enable(RCC_USART1);
    rcc_periph_clock_enable(RCC_SYSCFG);

    rcc_set_sysclk_source(RCC_CFGR_SW_PLL); /* careful with the param here! */
    rcc_wait_for_sysclk_status(RCC_PLL);
    /* FIXME - eventually handled internally */
    rcc_ahb_frequency = 80e6;
    rcc_apb1_frequency = 80e6;
    rcc_apb2_frequency = 80e6;
}

static void usart_setup(void)
{
    /* Setup GPIO pins for USART1 transmit. */
    gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO6|GPIO7);

    /* Setup USART1 TX/RX pin as alternate function. */
    gpio_set_af(GPIOB, GPIO_AF7, GPIO6 | GPIO7);

    usart_set_baudrate(USART_CONSOLE, 115200);
    usart_set_databits(USART_CONSOLE, 8);
    usart_set_stopbits(USART_CONSOLE, USART_STOPBITS_1);
    usart_set_mode(USART_CONSOLE, USART_MODE_TX_RX);
    usart_set_parity(USART_CONSOLE, USART_PARITY_NONE);
    usart_set_flow_control(USART_CONSOLE, USART_FLOWCONTROL_NONE);

    /* Finally enable the USART. */
    usart_enable(USART_CONSOLE);
}

/**
 * Use USART_CONSOLE as a console.
 * This is a syscall for newlib
 * @param file
 * @param ptr
 * @param len
 * @return
 */
int _write(int file, char *ptr, int len)
{
    int i;

    if (file == STDOUT_FILENO || file == STDERR_FILENO) {
        for (i = 0; i < len; i++) {
            if (ptr[i] == '\n') {
                usart_send_blocking(USART_CONSOLE, '\r');
            }
            usart_send_blocking(USART_CONSOLE, ptr[i]);
        }
        return i;
    }
    errno = EIO;
    return -1;
}

static void set_led(bool on) {
    if (on) {
        gpio_set(LED_1_PORT, LED_1_PIN);
    } else {
        gpio_clear(LED_1_PORT, LED_1_PIN);
    }
}

void read_from_console(char* str, int count) {
    for (int i = 0; i < count - 1; ++i) {
        char c = usart_recv_blocking(USART_CONSOLE);
        if (c == '\r') {
            str[i+1] = '\0';
            return;
        }
        str[i] = c;
    }
    str[count - 1] = '\0';
}

#define INPUT_BUFFER_SIZE 10

int main(void)
{
    int j = 0;
    clock_setup();
    usart_setup();

    printf("Hello\n");

    gpio_mode_setup(LED_1_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE,
            LED_1_PIN);

    set_led(true);

    static char buff[INPUT_BUFFER_SIZE];

    while (1) {
        memset(buff, 0, INPUT_BUFFER_SIZE);
        read_from_console(buff, INPUT_BUFFER_SIZE);
        /* printf("READ: |%s|\n", buff); */
        if (strcmp("on", buff) == 0) {
            set_led(true);
            printf("Led turned on\n");
        }
        else if (strcmp("off", buff) == 0) {
            set_led(false);
            printf("Led turned off\n");
        } else {
            printf("Invalid input\n");
        }
    }

    return 0;
}
