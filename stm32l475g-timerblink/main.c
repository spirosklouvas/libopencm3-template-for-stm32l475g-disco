#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/usart.h>

#define LED_1_PORT GPIOA
#define LED_1_PIN GPIO5

#define USART_CONSOLE USART1  /* PB6/7 , af7 */

int _write(int file, char *ptr, int len);

static void clock_setup(void)
{
    /* FIXME - this should eventually become a clock struct helper setup */
    rcc_osc_on(RCC_HSI16);

    flash_prefetch_enable();
    flash_set_ws(4);
    flash_dcache_enable();
    flash_icache_enable();
    /* 16MHz / 4 = > 4 * 40 = 160MHz VCO => 80MHz main pll  */
    rcc_set_main_pll(RCC_PLLCFGR_PLLSRC_HSI16, 4, 40,
            0, 0, RCC_PLLCFGR_PLLR_DIV2);
    rcc_osc_on(RCC_PLL);
    /* either rcc_wait_for_osc_ready() or do other things */

    /* Enable clocks for the ports we need */
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);

    /* Enable clocks for peripherals we need */
    rcc_periph_clock_enable(RCC_USART1);
    rcc_periph_clock_enable(RCC_TIM2);
    rcc_periph_clock_enable(RCC_SYSCFG);

    rcc_set_sysclk_source(RCC_CFGR_SW_PLL); /* careful with the param here! */
    rcc_wait_for_sysclk_status(RCC_PLL);
    /* FIXME - eventually handled internally */
    rcc_ahb_frequency = 80e6;
    rcc_apb1_frequency = 80e6;
    rcc_apb2_frequency = 80e6;
}

static void usart_setup(void)
{
    /* Setup GPIO pins for USART1 transmit. */
    gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO6|GPIO7);

    /* Setup USART1 TX pin as alternate function. */
    gpio_set_af(GPIOB, GPIO_AF7, GPIO6);

    usart_set_baudrate(USART_CONSOLE, 115200);
    usart_set_databits(USART_CONSOLE, 8);
    usart_set_stopbits(USART_CONSOLE, USART_STOPBITS_1);
    usart_set_mode(USART_CONSOLE, USART_MODE_TX);
    usart_set_parity(USART_CONSOLE, USART_PARITY_NONE);
    usart_set_flow_control(USART_CONSOLE, USART_FLOWCONTROL_NONE);

    /* Finally enable the USART. */
    usart_enable(USART_CONSOLE);
}

/**
 * Use USART_CONSOLE as a console.
 * This is a syscall for newlib
 * @param file
 * @param ptr
 * @param len
 * @return
 */
int _write(int file, char *ptr, int len)
{
    int i;

    if (file == STDOUT_FILENO || file == STDERR_FILENO) {
        for (i = 0; i < len; i++) {
            if (ptr[i] == '\n') {
                usart_send_blocking(USART_CONSOLE, '\r');
            }
            usart_send_blocking(USART_CONSOLE, ptr[i]);
        }
        return i;
    }
    errno = EIO;
    return -1;
}

static void tim2_setup(void) {
    nvic_enable_irq(NVIC_TIM2_IRQ);
    nvic_set_priority(NVIC_TIM2_IRQ, 1);
    rcc_periph_reset_pulse(RST_TIM2);

    TIM_CNT(TIM2) = 1;
    // 50000 counts every second
    timer_set_prescaler(TIM2, rcc_apb1_frequency / 50000);
    timer_set_period(TIM2, 50000);

    timer_enable_irq(TIM2, TIM_DIER_UIE);

    timer_enable_counter(TIM2);
}

static void led1_toggle(void) {
    gpio_toggle(LED_1_PORT, LED_1_PIN);
}

static int counter = 0;

static void print_counter(void) {
    printf("Counter: %d\n", counter);
}

void tim2_isr(void) {
    led1_toggle();
    ++counter;
    print_counter();

    timer_clear_flag(TIM2, TIM_SR_UIF); // clear interrrupt flag
}

int main(void)
{
    int j = 0;
    clock_setup();
    usart_setup();

    printf("Hello\n");
    print_counter();

    tim2_setup();

    gpio_mode_setup(LED_1_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE,
            LED_1_PIN);

    while (1) {
        __asm__("WFI");
    }

    return 0;
}
